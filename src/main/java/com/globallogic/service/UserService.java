package com.globallogic.service;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.model.IUser;
import com.globallogic.model.impl.User;
import com.globallogic.util.Util;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserService {
	
	@Autowired
	private IUser iUser;
	@Autowired
	private Util util;
	
	public void setUser(User user) {
		
		if (util.validateEmailFormat(user.getEmail())) {
			if (util.validatePasswordFormat(user.getPassword())) { 
				if (iUser.findByEmail(user.getEmail()).isEmpty()) {
					
					user.setId(UUID.randomUUID().toString());
					user.setCreated(LocalDateTime.now());
					user.setModified(LocalDateTime.now());
					user.setLastLogin(LocalDateTime.now());
					user.setActive(true);
					
					iUser.save(user);
					log.info("Usuario Agregado");
					
				} else {
					log.warn("El correo ya esta registrado"); //TODO: Devolver como JSon
				}
			} else {
				log.warn("Formato de contraseña invalido");//TODO: Devolver como JSon
			}
		} else {
			log.warn("Formato de correo invalido");//TODO: Devolver como JSon
		}
	}
}