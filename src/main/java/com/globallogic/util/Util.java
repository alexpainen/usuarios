package com.globallogic.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class Util {

	public boolean validateEmailFormat(String email) {

		Pattern pattern = Pattern.compile(
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		Matcher mather = pattern.matcher(email);

		if (mather.find() == true) {
			return true;
		} else {
			return false;
		}
	}

	public boolean validatePasswordFormat(String password) {

		Pattern pattern = Pattern.compile("^(?=.+[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$");
		Matcher mather = pattern.matcher(password);

		if (mather.find() == true) {
			return true;
		} else {
			return false;
		}
	}
}
