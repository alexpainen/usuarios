package com.globallogic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.globallogic.model.IUser;
import com.globallogic.model.impl.User;
import com.globallogic.service.UserService;

@RestController
@RequestMapping("/ussers")
public class UserController {
	
	@Autowired
	private IUser iUser;
	@Autowired
	private UserService userService;

	@GetMapping
	public List<User> listar(){
		return iUser.findAll();
	}
	
	@PostMapping
	public void insertar(@RequestBody User user){
		iUser.saveAndFlush(user);
	}
	
	@PutMapping
	public void modificar(@RequestBody User user){
//		iUser.save(user);
		userService.setUser(user);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id){
		iUser.deleteById(id);
	}
}