package com.globallogic.model.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity (name = "Phones")
public class Phone {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "phone_id")
	private String id;
	private int number;
	@Column(name = "city_code")
	private int cityCode;
	@Column(name = "country_code")
	private int countryCode;
}