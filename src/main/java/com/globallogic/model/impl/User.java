package com.globallogic.model.impl;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name = "Ussers")
public class User {
	
	@Id
	@Column(name = "user_id")
	private String id;
	private String name;
	private String email;
	private String password;
	private LocalDateTime created;
	private LocalDateTime modified;
	@Column(name = "last_login")
	private LocalDateTime lastLogin;
	private String token;
	@Column(name = "is_active")
	private boolean isActive;
	@OneToMany(cascade = CascadeType.ALL)
	private Set<Phone> phones;	
}