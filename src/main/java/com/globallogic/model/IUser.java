package com.globallogic.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.globallogic.model.impl.User;

public interface IUser extends JpaRepository<User, Integer>{
	
	List<User> findByEmail(String email);
	
}