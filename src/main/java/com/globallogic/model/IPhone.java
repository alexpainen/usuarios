package com.globallogic.model;

import org.springframework.data.jpa.repository.JpaRepository;

import com.globallogic.model.impl.Phone;

public interface IPhone extends JpaRepository<Phone, Integer> {

}
